# json.partial! "vocabularies/vocabulary", vocabulary: @vocabulary
json.array! [@vocabulary.id, @vocabulary.word, @vocabulary.vocalize, @vocabulary.translate, @vocabulary.example]
