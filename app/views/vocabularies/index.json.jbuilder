# json.array! @vocabularies, partial: "vocabularies/vocabulary", as: :vocabulary
json.vocabularies @vocabularies.each do |vocabulary|
    json.id vocabulary.id
    json.word vocabulary.word
    json.vocalize vocabulary.vocalize
    json.translate vocabulary.translate
    json.example vocabulary.example
    # json.user_id vocabulary.user_id
end
