# json.array! @notes, partial: "notes/note", as: :note

json.notes @notes do |note|
    json.id note.id
    json.title note.title
    json.content note.content
    json.bg_color note.bgColor
end
