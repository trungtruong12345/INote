import Vue from 'vue'
import Vuex from 'vuex'
import { createNote, notes, updateNote, deleteNote } from './api/notes.js'
import { vocabularies, createVocabulary, updateVocabulary, deleteVocabulary } from './api/vocabularies.js'

Vue.use(Vuex)


const module_notes = {
    namespaced: true,
    state: {
        notes: [],
        search: '',
    },
    mutations: {
        get(state, { data }) {
            state.notes = data
        },
        search(state, { value }) {
            state.search = value
        }
    },
    actions: {
        getNotes(context) {
            notes(context.state.search).then((res) => {
                context.commit('get', { data: res.data.notes })
            })
        },
        createNote(context, { title, bgColor, content }) {
            context.commit('search', { value: '' })
            createNote(title, content, bgColor)
                .then(() => {
                    this.dispatch('notes/getNotes')
                })
        },
        updateNote(context, { title, bgColor, content, id }) {
            updateNote(title, content, bgColor, id)
                .then(() => {
                    this.dispatch('notes/getNotes')
                })
        },
        deleteNote(context, { id }) {
            deleteNote(id)
                .then(() => {
                    this.dispatch('notes/getNotes')
                })
        }
    }
}




const module_vocabularies = {
    namespaced: true,
    state: {
        vocabularies: [],
        search: '',
    },
    mutations: {
        get(state, { data }) {
            state.vocabularies = data
        },
        search(state, { value }) {
            state.search = value
        }
    },
    actions: {
        getVocabularies(context) {
            vocabularies(context.state.search).then((res) => {
                context.commit('get', { data: res.data.vocabularies })
            })
        },
        createVocabulary(context, { word, vocalize, translate, example }) {
            context.commit('search', { value: '' })
            createVocabulary(word, vocalize, translate, example)
                .then(() => {
                    this.dispatch('vocabularies/getVocabularies')
                })
        },
        updateVocabulary(context, { word, vocalize, translate, example, id }) {
            updateVocabulary(word, vocalize, translate, example, id)
                .then(() => {
                    this.dispatch('vocabularies/getVocabularies')
                })
        },
        deleteVocabulary(context, { id }) {
            deleteVocabulary(id)
                .then(() => {
                    this.dispatch('vocabularies/getVocabularies')
                })
        }
    }
}

export default new Vuex.Store({
    modules: {
        notes: module_notes,
        vocabularies: module_vocabularies,
    }
})