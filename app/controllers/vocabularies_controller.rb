class VocabulariesController < ApplicationController
  before_action :set_vocabulary, only: %i[ show update destroy ]

  # GET /vocabularies
  # GET /vocabularies.json
  def index
    @vocabularies = Vocabulary.where("word like ? or translate like ?", "%#{params[:search]}%", "%#{params[:search]}%").order(id: :DESC)
  end

  # GET /vocabularies/1
  # GET /vocabularies/1.json
  def show
  end

  # POST /vocabularies
  # POST /vocabularies.json
  def create
    @vocabulary = Vocabulary.new(vocabulary_params)

    if @vocabulary.save
      render :show, status: :created, location: @vocabulary
    else
      render json: @vocabulary.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vocabularies/1
  # PATCH/PUT /vocabularies/1.json
  def update
    if @vocabulary.update(vocabulary_params)
      render :show, status: :ok, location: @vocabulary
    else
      render json: @vocabulary.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vocabularies/1
  # DELETE /vocabularies/1.json
  def destroy
    @vocabulary.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vocabulary
      @vocabulary = Vocabulary.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def vocabulary_params
      params.require(:vocabulary).permit(:word, :vocalize, :translate, :example, :user_id)
    end
end
