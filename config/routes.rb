Rails.application.routes.draw do
  scope :api do
    resources :notes
    resources :vocabularies
  end

  get '/*path', to: 'static_page#index'
  
  root 'static_page#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
