# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# (1..5).each do |i|
#     Note.create(title: (0...8).map { (65 + rand(26)).chr }.join, content: 50.times.map { (0...(rand(10))).map { ('a'..'z').to_a[rand(26)] }.join }.join(" "), user_id: i, bgColor: ['#fef175', '#f28b82', '#e6c9a8', 'white'].sample())
# end


(1..15).each do |i|
    Vocabulary.create(word: :hello, vocalize: "/hə'ləʊ/", translate: "Xin Chao", example: "hello, how are you ?", user_id: 1)
end